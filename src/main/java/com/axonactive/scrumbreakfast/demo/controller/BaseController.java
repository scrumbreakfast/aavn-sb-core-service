package com.axonactive.scrumbreakfast.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Created by vhphat on 9/10/2015.
 */
public class BaseController {
    private static Logger logger = LoggerFactory.getLogger(BaseController.class);

    protected Map<String, Object> resultMap(final Boolean success, final Object result) {
        return new HashMap<String, Object>(){{
            put("success", success.toString());
            if(result != null) {
                put("result", result);
            }
        }};
    }

    @ExceptionHandler(Exception.class)
    public Object handleException(Exception exception) {
        logger.error("Exception processing request", exception);
        Map<String, Object> result = new HashMap<>();
        result.put("result", null);
        result.put("success", "false");
        result.put("error", exception.getMessage());
        return new ResponseEntity(result, HttpStatus.OK);
    }
}
