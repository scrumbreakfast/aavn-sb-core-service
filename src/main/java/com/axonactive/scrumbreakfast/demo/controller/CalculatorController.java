package com.axonactive.scrumbreakfast.demo.controller;

import com.axonactive.scrumbreakfast.demo.enums.MathOperation;
import com.axonactive.scrumbreakfast.demo.service.CalculatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by vhphat on 9/9/2015.
 */
@RestController
@RequestMapping(value = "/calculator")
public class CalculatorController extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(CalculatorController.class);

    @Autowired
    CalculatorService calculatorService;

    @RequestMapping(value = "/{operation}", method = RequestMethod.GET)
    public Object addTwoNumbers(@PathVariable("operation") MathOperation operation,
                                @RequestParam(value = "num1") double num1,
                                @RequestParam(value = "num2") double num2,
                                @RequestParam(value = "scale", required = false) Integer scale){

        logger.info("/calculator/{}?num1={}&num2={}&scale={}", operation.name(), num1, num2, scale);
        double result = calculatorService.operate(operation, num1, num2, scale);

        return resultMap(true, result);
    }
}
