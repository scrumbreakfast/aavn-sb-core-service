package com.axonactive.scrumbreakfast.demo.enums;

/**
 * Created by vhphat on 9/10/2015.
 */
public enum MathOperation {
    add, minus, multiply, divide
}
