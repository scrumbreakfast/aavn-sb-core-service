package com.axonactive.scrumbreakfast.demo.service;

import com.axonactive.scrumbreakfast.demo.enums.MathOperation;

/**
 * Created by vhphat on 9/10/2015.
 */
public interface CalculatorService {
    double operate(MathOperation operation, double num1, double num2, Integer scale);
}
