package com.axonactive.scrumbreakfast.demo.service;

/**
 * Created by vhphat on 9/9/2015.
 */
public interface MathService {
    double add(double num1, double num2);
    double minus(double num1, double num2);
    double multiply(double num1, double num2);

    double divide(double num1, double num2);
    double divide(double num1, double num2, Integer scale);
}
