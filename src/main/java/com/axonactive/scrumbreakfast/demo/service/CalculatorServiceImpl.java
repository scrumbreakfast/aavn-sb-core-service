package com.axonactive.scrumbreakfast.demo.service;

import com.axonactive.scrumbreakfast.demo.enums.MathOperation;

/**
 * Created by vhphat on 9/10/2015.
 */
public class CalculatorServiceImpl implements CalculatorService{
    private MathService mathService;
    public CalculatorServiceImpl(MathService mathService) {
        this.mathService = mathService;
    }

    public double operate(MathOperation operation, double num1, double num2, Integer scale) {
        switch (operation) {
            case add: return mathService.add(num1, num2);
            case minus: return mathService.minus(num1, num2);
            case multiply: return mathService.multiply(num1, num2);
            case divide: return mathService.divide(num1, num2, scale);

            default: return 0l;
        }
    }
}
