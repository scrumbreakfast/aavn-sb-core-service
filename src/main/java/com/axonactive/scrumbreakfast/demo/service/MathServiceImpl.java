package com.axonactive.scrumbreakfast.demo.service;

import java.math.BigDecimal;

/**
 * Created by vhphat on 9/9/2015.
 */
public class MathServiceImpl implements MathService {
    public double add(double num1, double num2) {
        return num1 + num2;
    }

    public double minus(double num1, double num2) {
        return num1 - num2;
    }

    public double multiply(double num1, double num2) {
        return num1 * num2;
    }

    public double divide(double num1, double num2, Integer scale) {
        if (num2 == 0) {
            throw new ArithmeticException("Division by zero error.");
        }
        if (scale == null) {
            return num1 / num2;
        } else {
            BigDecimal bgNum1 = new BigDecimal(num1);
            BigDecimal bgNum2 = new BigDecimal(num2);
            return bgNum1.divide(bgNum2, scale, BigDecimal.ROUND_UP).doubleValue();
        }
    }
    public double divide(double num1, double num2) {
        return this.divide(num1, num2, null);
    }
}
