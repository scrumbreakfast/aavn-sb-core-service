package com.axonactive.scrumbreakfast.demo.service;

import com.axonactive.scrumbreakfast.demo.enums.MathOperation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
/**
 * Created by vhphat on 9/10/2015.
 */
@RunWith(MockitoJUnitRunner.class)
public class CalculatorServiceImplTest {

    @Mock
    MathService mathService;

    @InjectMocks
    CalculatorServiceImpl calculatorService;

    @Test
    public void testAdd() {
        when(mathService.add(1, 2)).thenReturn(3.0);
        double result = calculatorService.operate(MathOperation.add, 1, 2, null);
        assertEquals(3, result, 0);
    }

    @Test
    public void testMinus() {
        when(mathService.minus(1, 2)).thenReturn(-1.0);
        double result = calculatorService.operate(MathOperation.minus, 1, 2, null);
        assertEquals(-1.0, result, 0);
    }

    @Test
    public void testMultiply() {
        when(mathService.multiply(1, 2)).thenReturn(2.0);
        double result = calculatorService.operate(MathOperation.multiply, 1, 2, null);
        assertEquals(2, result, 0);
    }

    @Test
    public void testDivide() {
        when(mathService.divide(1, 2, null)).thenReturn(0.5);
        double result = calculatorService.operate(MathOperation.divide, 1, 2, null);
        assertEquals(0.5, result, 0);
    }

    @Test
    public void testDivide_with_scale() {
        when(mathService.divide(2.33, 1.12, 3)).thenReturn(2.081);
        double result = calculatorService.operate(MathOperation.divide, 2.33, 1.12, 3);
        assertEquals(2.081, result, 0);
    }

    @Test(expected=ArithmeticException.class)
    public void testDivide_Exception() {
        when(mathService.divide(1, 0, null)).thenThrow(ArithmeticException.class);
        calculatorService.operate(MathOperation.divide, 1, 0, null);
    }
}
