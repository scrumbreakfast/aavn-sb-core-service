package com.axonactive.scrumbreakfast.demo.service;

import com.axonactive.scrumbreakfast.demo.MathDataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
/**
 * Created by vhphat on 9/10/2015.
 */

@RunWith(DataProviderRunner.class)
public class MathServiceImplTest {
    MathService mathService = new MathServiceImpl();

    @Test
    @UseDataProvider(value = "dataProviderAdd", location = MathDataProvider.class)
    public void testAdd(double num1, double num2, double expected) {
        double result = mathService.add(num1, num2);
        assertEquals(expected, result, 0);
    }

    @Test
    @UseDataProvider(value = "dataProviderMinus", location = MathDataProvider.class)
    public void testMinus(double num1, double num2, double expected) {
        double result = mathService.minus(num1, num2);
        assertEquals(expected, result, 0);
    }

    @Test
    @UseDataProvider(value = "dataProviderMultiply", location = MathDataProvider.class)
    public void testMultiply(double num1, double num2, double expected) {
        double result = mathService.multiply(num1, num2);
        assertEquals(expected, result, 0.01);
    }

    @Test
    @UseDataProvider(value = "dataProviderDivide", location = MathDataProvider.class)
    public void testDivide(double num1, double num2, double expected) {
        double result = mathService.divide(num1, num2);
        assertEquals(expected, result, 0.01);
    }

    @Test(expected = ArithmeticException.class)
    public void testDivide_Exception() {
        // division by zero exception.
        mathService.divide(1, 0);
    }
}
