package com.axonactive.scrumbreakfast.demo;

import com.tngtech.java.junit.dataprovider.DataProvider;

/**
 * Created by vhphat on 9/10/2015.
 */
public class MathDataProvider {
    @DataProvider
    public static Object[][] dataProviderAdd() {
        return new Object[][] {
                { 0, 0, 0 },
                { 1, 1, 2 },
                { -1, 1, 0 },
                { 1.5, 2.9, 4.4 },
                { -88, 23, -65 },
        };
    }

    @DataProvider
    public static Object[][] dataProviderMinus() {
        return new Object[][] {
                { 0, 0, 0 },
                { 1, 1, 0 },
                { -1, 1, -2 },
                { 4.5, 2.33, 2.17 },
                { -88, 23, -111 },
        };
    }

    @DataProvider
    public static Object[][] dataProviderMultiply() {
        return new Object[][] {
                { 0, 0, 0 },
                { 1, 1, 1 },
                { 1.99999, 0.1212, 0.242398788 },
                { 99, 2, 198 },
        };
    }

    @DataProvider
    public static Object[][] dataProviderDivide() {
        return new Object[][] {
                { 1, 1, 1 },
                { 0, 1, 0 },
                { 4, 2, 2 },
                { 3.151515, 1.1212, 2.81084106 },
                { -4, 8, -0.50 },
        };
    }
}
